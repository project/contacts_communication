<?php

/**
 * @file
 * Views hooks.
 */

/**
 * Implements hook_views_data().
 */
function contacts_communication_views_data() {
  $data['views']['contacts_communication_new'] = [
    'title' => t('New Communication Links'),
    'help' => t('Displays links to create new communications.'),
    'area' => [
      'id' => 'contacts_communication_new',
    ],
  ];
  return $data;
}
