<?php

namespace Drupal\contacts_communication\Controller;

use Drupal\communication\CommunicationBuilderInterface;
use Drupal\communication\CommunicationModePluginManager;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contacts communications pages.
 *
 * @package Drupal\contacts_communication\Controller
 */
class ContactsCommunicationController extends ControllerBase {

  /**
   * Communication Mode Plugin Manager.
   *
   * @var \Drupal\communication\CommunicationModePluginManager
   */
  protected $modeManager;

  /**
   * Communication Builder.
   *
   * @var \Drupal\communication\CommunicationBuilderInterface
   */
  protected $communicationBuilder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.communication.mode'),
      $container->get('communication.builder')
    );
  }

  /**
   * ContactsCommunicationController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\communication\CommunicationModePluginManager $mode_manager
   *   Communication Mode manager.
   * @param \Drupal\communication\CommunicationBuilderInterface $communication_builder
   *   Communication builder.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CommunicationModePluginManager $mode_manager, CommunicationBuilderInterface $communication_builder) {
    $this->entityTypeManager = $entity_type_manager;
    $this->modeManager = $mode_manager;
    $this->communicationBuilder = $communication_builder;
  }

  /**
   * Create and send a communication.
   *
   * @param \Drupal\user\UserInterface $user
   *   The recipient user of the communication.
   * @param string $mode
   *   The communication mode.
   * @param \Drupal\user\UserInterface|null $source
   *   The sender user.
   *
   * @return array
   *   Form build array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function send(UserInterface $user, string $mode, UserInterface $source = NULL) {
    if (!$source) {
      $source = $this->entityTypeManager->getStorage('user')->load(
        $this->currentUser()->id()
      );
    }

    $communication = $this->communicationBuilder->buildCommunication($mode, $user, $source);
    return $this->entityFormBuilder()->getForm($communication, 'add');
  }

  /**
   * Get the access control for sending a comm.
   *
   * @param \Drupal\user\UserInterface $user
   *   The recipient user.
   * @param string $mode
   *   The mode name.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   AccessResult for permission so send a communication to this user.
   */
  public function sendAccess(UserInterface $user, string $mode) {
    return AccessResult::allowedIf($this->modeManager->hasDefinition($mode));
  }

}
