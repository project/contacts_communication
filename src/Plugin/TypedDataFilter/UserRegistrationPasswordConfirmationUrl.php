<?php

namespace Drupal\contacts_communication\Plugin\TypedDataFilter;

use Drupal\communication_user\Plugin\TypedDataFilter\UserRegistrationPasswordConfirmationUrl as BaseUserRegistrationPasswordConfirmationUrl;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\TypedData\DataDefinitionInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Filter to make a regtistration confirmation link.
 *
 * This needs the user_registrationpassword module to be enabled, without that
 * module it will fallback to the core drupal one time login link.
 *
 * @DataFilter(
 *   id = "registration_password_destination_url",
 *   label = @Translation("Provides the one time login url for this user with destination."),
 * )
 *
 * @package Drupal\communication_user\Plugin\TypedDataFilter
 */
class UserRegistrationPasswordConfirmationUrl extends BaseUserRegistrationPasswordConfirmationUrl implements ContainerFactoryPluginInterface {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')
    );
  }

  /**
   * UserRegistrationPasswordConfirmationUrl constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, RequestStack $request_stack) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function filter(
    DataDefinitionInterface $definition,
    $value,
    array $arguments,
    BubbleableMetadata $bubbleable_metadata = NULL
  ) {
    assert($value instanceof UserInterface);
    $url = is_callable('user_registrationpassword_confirmation_url') ? user_registrationpassword_confirmation_url($value) : user_pass_reset_url($value);

    // Use the destination from the query parameters, as if we have no
    // destination we want to let it behave as normal.
    if ($destination = $this->request->query->get('destination')) {
      $url .= '?destination=' . $destination;
    }

    return $url;
  }

}
