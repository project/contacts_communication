<?php

namespace Drupal\contacts_communication\Plugin\views\area;

use Drupal\Core\Url;
use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * A views area showing links to create new communications.
 *
 * @ViewsArea("contacts_communication_new")
 *
 * @package Drupal\contacts_communication\Plugin\views\area
 */
class NewCommunicationLinks extends AreaPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    $user_id = NULL;
    foreach ($this->view->argument as $argument) {
      if ($argument->field === 'contact') {
        $value = $argument->getValue();
        $user_id = is_array($value) ? reset($value) : $value;
        break;
      }
    }

    /** @var \Drupal\communication\CommunicationModePluginManager $mode_manager */
    $mode_manager = \Drupal::service('plugin.manager.communication.mode');

    $build = [
      '#type' => 'container',
    ];

    $build['template'] = [
      '#type' => 'link',
      '#title' => $this->t('New from Template'),
      '#url' => Url::fromRoute(
        'contacts.communication.template',
        [
          'user' => $user_id,
        ],
        [
          'query' => \Drupal::destination()->getAsArray(),
        ]
      ),
      '#attributes' => [
        'class' => ['btn'],
      ],
    ];

    foreach ($mode_manager->getDefinitions() as $mode => $mode_def) {
      $build[$mode] = [
        '#type' => 'link',
        '#title' => $this->t('New @mode', ['@mode' => $mode_def['label']]),
        '#url' => Url::fromRoute(
          'contacts.communication.send',
          [
            'user' => $user_id,
            'mode' => $mode,
          ],
          [
            'query' => \Drupal::destination()->getAsArray(),
          ]
        ),
        '#attributes' => [
          'class' => ['btn'],
        ],
      ];
    }

    return $build;
  }

}
