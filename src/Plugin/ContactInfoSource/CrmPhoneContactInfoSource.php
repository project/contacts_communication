<?php

namespace Drupal\contacts_communication\Plugin\ContactInfoSource;

use Drupal\communication\Contact\ContactInfo;
use Drupal\communication\Contact\ContactInfoDefinitionInterface;
use Drupal\communication_user\Plugin\ContactInfoSource\ProfileFieldSource;
use Drupal\Core\Entity\EntityInterface;

/**
 * Get phone contact info from the crm_phone field.
 *
 * @ContactInfoSource(
 *   id = "crm_phone",
 *   label = @Translation("CRM Phone Field"),
 *   entity_type_id = "user",
 * )
 *
 * @package Drupal\contacts_communication\Plugin\Communication\ContactInfoSource
 */
class CrmPhoneContactInfoSource extends ProfileFieldSource {

  /**
   * {@inheritdoc}
   */
  public function collectInfo(EntityInterface $entity, ContactInfoDefinitionInterface $definition, array $options = []) {
    $profile_storage = $this->entityTypeManager->getStorage('profile');
    $ids = $profile_storage->getQuery()
      ->condition('uid', $entity->id())
      ->condition('is_default', 1)
      ->execute();

    $info = [];
    /** @var \Drupal\profile\Entity\Profile $profile */
    foreach ($profile_storage->loadMultiple($ids) as $profile) {
      $bundle = $profile->bundle();

      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
      foreach ($profile->getFieldDefinitions() as $field_definition) {
        $sub_key = "{$bundle}.{$field_definition->getName()}";
        if ($field_definition->getName() == 'crm_phone' && $definition->getDataType() == 'telephone') {
          $info[$sub_key] = new ContactInfo(
            $definition,
            $entity,
            $this->getPluginId(),
            $sub_key
          );
          if ($profile->{$field_definition->getName()}->isEmpty()) {
            $info[$sub_key]->setIncomplete();
          }
        }
      }
    }

    return $info;
  }

}
